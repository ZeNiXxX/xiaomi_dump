#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:59364686:6259299ce89c0257218214d1600dc440f8f12de9; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:45540682:984ae61443170ea952b1aae4a2fcbed593716a44 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:59364686:6259299ce89c0257218214d1600dc440f8f12de9 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
